#include "AnimatedSprite.h"

#include <SDL.h>
#include "GameObject.h"
#include "Camera.h"
#include "SpriteSheet.h"
#include <iostream>


AnimatedSprite::AnimatedSprite(Camera* aCamera, SpriteSheet* aSpriteSheet, const std::string& startingSequence) :
	Sprite(aCamera), _spriteSheet(aSpriteSheet)
{
	PlaySequence(startingSequence);
}

AnimatedSprite::~AnimatedSprite()
{
}

void AnimatedSprite::PlaySequence(const std::string& name)
{
	if (_currentSequence == name) return;
	_index = 0;
	_currentSequence = name;
	_frameLength = _spriteSheet->_sequences[name]._frameTime;
}

void AnimatedSprite::Update(int dt)
{
	if (_currentSequence == "") return;
	_timer += dt;
	if (_timer > _frameLength)
	{
		_timer = _timer % _frameLength;
		_index++;
		_index %= _spriteSheet->_sequences[_currentSequence]._frames.size();

		_srcRect = &_spriteSheet->_sequences[_currentSequence]._frames[_index];
	}
}

void AnimatedSprite::Draw()
{
	SDL_Rect destRect;
	if (_camera != nullptr)
	{
		destRect = { (int)(_gameObject->GetDrawPositionX() - _camera->x), (int)(_gameObject->GetDrawPositionY() - _camera->y), _spriteSheet->_width, _spriteSheet->_height };
	}
	else
	{
		destRect = { (int)_gameObject->GetDrawPositionX(), (int)_gameObject->GetDrawPositionY(), _spriteSheet->_width, _spriteSheet->_height };
	}

	SDL_RenderCopyEx(_renderer, _spriteSheet->_texture, _srcRect, &destRect, 0.0f, NULL, (SDL_RendererFlip)_flip);
}
