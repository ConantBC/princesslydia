#pragma once

#include "Sprite.h"

#include <string>
#include <vector>
#include <map>

#ifdef BENTOBONITOENGINE_EXPORTS
#define BENTO_EXPORT __declspec(dllexport)
#else
#define BENTO_EXPORT __declspec(dllimport)
#endif

class GameObject;
struct Camera;
struct Sequence;
struct SpriteSheet;
struct SDL_Rect;

class BENTO_EXPORT AnimatedSprite : public Sprite
{
public:
	AnimatedSprite(Camera* aCamera, SpriteSheet* aSpriteSheet, const std::string& startingSequence);
	~AnimatedSprite();

	void PlaySequence(const std::string& name);

	void Update(int dt);
	void Draw();

private:
	SpriteSheet* _spriteSheet = nullptr;
	SDL_Rect* _srcRect;
	std::string _currentSequence;
	int _frameLength = 1000;
	int _index = 0;
	int _timer = 0;
};


