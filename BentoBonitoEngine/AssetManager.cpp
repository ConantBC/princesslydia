#include "AssetManager.h"

AssetManager::AssetManager()
{
}

AssetManager::~AssetManager()
{
}

Tileset* AssetManager::LoadTileset(const std::string& aTileSet)
{
	if (_tilesets.count(aTileSet) == 1)
	{
		return &_tilesets[aTileSet];
	}
	return nullptr;
}
