#pragma once

#include "Tileset.h"

#include <map>
#include <string>

class AssetManager
{
public:
	AssetManager();
	~AssetManager();

	Tileset* LoadTileset(const std::string& aTileSet);

private:
	std::map<std::string, Tileset> _tilesets;
};

