#include <SDL.h>
#include "tinyxml2.h"

#include "BentoBonitoEngine.h"
#include "Clock.h"
#include "GameData.h"

GameData* _pData;

void Bento_CreateGame(std::string title, int width, int height, int scale)
{
	_pData = new GameData(title, width, height, scale, false);
	SDL_Init(SDL_INIT_VIDEO);
	_pData->renderManager.CreateWindow();
}

void Bento_CreateGame()
{
	tinyxml2::XMLDocument config;
	config.LoadFile("assets/GameConfiguration.xml");
	tinyxml2::XMLElement* game = config.FirstChildElement("Game");
	std::string title;
	int width, height;
	int scale;
	bool fullscreen;
	bool drawGizmos;

	game->QueryIntAttribute("width", &width);
	game->QueryIntAttribute("height", &height);
	game->QueryIntAttribute("scale", &scale);
	game->QueryBoolAttribute("fullscreen", &fullscreen);
	game->QueryBoolAttribute("drawgizmos", &drawGizmos);

	_pData = new GameData("Princess Lydia", width, height, scale, drawGizmos);
	SDL_Init(SDL_INIT_VIDEO);
	_pData->renderManager.CreateWindow();
}

void Bento_Run()
{
	Clock time;
	_pData->isRunning = true;

	_pData->gameManager.Start();

	// Game Loop
	while (_pData->isRunning)
	{
		time.Update();

		_pData->inputManager.Update();

		_pData->gameManager.Update(time.deltaTime);
		_pData->gameManager.Draw();

		SDL_Delay(2);
	}

	SDL_Quit();
}

void Bento_Close()
{
	delete _pData;
}

void Bento_AddScene(const std::string& aName, Scene* aScene)
{
	_pData->gameManager.AddScene(aName, aScene);
}

void Bento_LoadScene(const std::string& aName, const std::string& aFilePath)
{
	_pData->gameManager.AddScene(aName, new Scene(aName, aFilePath));
}

void Bento_SetBackgroundColor(int r, int g, int b)
{
	_pData->renderManager.SetBackgroundColor(r, g, b);
}

//TODO: Un-hard this code

bool Bento_GetLeft() { return _pData->inputManager.GetLeft(); }
bool Bento_GetLeftDown() { return _pData->inputManager.GetLeftDown(); }
bool Bento_GetRight() { return _pData->inputManager.GetRight(); }
bool Bento_GetRightDown() { return _pData->inputManager.GetRightDown(); }

struct Vector2
{
	Vector2() : x(0), y(0) {}
	Vector2(int x, int y) : x(x), y(y) {}

	Vector2 operator+(const Vector2& rhs)
	{
		return Vector2(x + rhs.x, y + rhs.y);
	}

	int x, y;
};