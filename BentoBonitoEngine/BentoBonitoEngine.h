#pragma once

#include <string>

#include "Scene.h"

#ifdef BENTOBONITOENGINE_EXPORTS
#define BENTO_EXPORT __declspec(dllexport)
#else
#define BENTO_EXPORT __declspec(dllimport)
#endif

struct BENTO_EXPORT Vector2;

/// Initializes the engine and creates a GameData object
BENTO_EXPORT void Bento_CreateGame(std::string title, int width, int height, int scale);
BENTO_EXPORT void Bento_CreateGame();

/// Creates a window and begins the main game loop
BENTO_EXPORT void Bento_Run();

/// Destroys the game data object and frees up any memory being used by Bento Bonito
BENTO_EXPORT void Bento_Close();


// Scene Management
BENTO_EXPORT void Bento_AddScene(const std::string& aName, Scene* aScene);
BENTO_EXPORT void Bento_LoadScene(const std::string& aName, const std::string& aFilePath);

// Graphics
BENTO_EXPORT void Bento_SetBackgroundColor(int r, int g, int b);

// Input
BENTO_EXPORT bool Bento_GetLeft();
BENTO_EXPORT bool Bento_GetLeftDown();
BENTO_EXPORT bool Bento_GetRight();
BENTO_EXPORT bool Bento_GetRightDown();