#pragma once

#ifdef BENTOBONITOENGINE_EXPORTS
#define BENTO_EXPORT __declspec(dllexport)
#else
#define BENTO_EXPORT __declspec(dllimport)
#endif

struct BENTO_EXPORT Camera
{
	Camera() : x(0), y(0), boundsX(0), boundsY(0) {}
	Camera(float x, float y) : x(x), y(y), boundsX(0), boundsY(0) {}
	
	void SetBounds(int x, int y)
	{
		boundsX = x;
		boundsY = y;
	}

	void Clamp()
	{
		if (x < 0) x = 0;
		if (x > boundsX) x = boundsX;
		if (y < 0) y = 0;
		if (y > boundsY) y = boundsY;
	}
	
	float x, y;
	float boundsX, boundsY;
};
