#pragma once

#include <SDL.h>

struct Clock
{
	int deltaTime = 0;
	int time = 0;

	Clock()
	{
		time = SDL_GetTicks();
	}

	void Update()
	{
		deltaTime = SDL_GetTicks() - time;
		time += deltaTime;
	}
};
