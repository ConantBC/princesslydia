#pragma once

#include "Component.h"

#include "GameObject.h"

#ifdef BENTOBONITOENGINE_EXPORTS
#define BENTO_EXPORT __declspec(dllexport)
#else
#define BENTO_EXPORT __declspec(dllimport)
#endif

class b2PolygonShape;

class Collider : public Component
{
public:
	Collider(GameObject* aGameObject) : Component(aGameObject) {}
	~Collider() {}

private:
	b2PolygonShape* _shape = nullptr;

};

