#pragma once

#ifdef BENTOBONITOENGINE_EXPORTS
#define BENTO_EXPORT __declspec(dllexport)
#else
#define BENTO_EXPORT __declspec(dllimport)
#endif

class GameObject;

class BENTO_EXPORT Component
{
public:
	Component(GameObject* pObject) : gameObject(pObject) {}
	virtual ~Component() {}
	virtual void Start() = 0;
	virtual void Update(int dt) = 0;
	virtual void OnCollisionEnter() = 0;
	virtual void OnCollisionExit() = 0;

protected:
	GameObject* gameObject = nullptr;
};

