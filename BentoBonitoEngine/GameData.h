#pragma once

#include <string>

#include "RenderManager.h"
#include "InputManager.h"
#include "GameManager.h"
#include "AssetManager.h"

struct GameData
{
	GameData(std::string title, int width, int height, int scale, bool drawGizmos) :
		renderManager(this, title, width, height, scale),
		inputManager(this), drawGizmos(drawGizmos)
	{
		if (singleton == nullptr)
		{
			singleton = this;
		}
		else
		{
			delete this;
		}
	}

	~GameData() {}

	static GameData* GetSingleton() { return GameData::singleton; }

	RenderManager renderManager;
	InputManager inputManager;
	GameManager gameManager;
	AssetManager assetManager;

	bool isRunning = false;
	bool drawGizmos = false;

private:
	static GameData* singleton;

};