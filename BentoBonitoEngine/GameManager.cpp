#include "GameManager.h"

#include "GameData.h"

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
}

void GameManager::Start()
{
	for (auto s : _scenes)
	{
		s.second->Start();
	}
}

void GameManager::Update(int dt)
{
	for (auto s : _scenes)
	{
		s.second->Update(dt);
	}
}

void GameManager::Draw()
{
	SDL_Renderer* renderer = GameData::GetSingleton()->renderManager.GetRenderer();

	SDL_RenderClear(renderer);

	for (auto s : _scenes)
	{
		s.second->Draw();
		if (drawGizmos) s.second->DrawGizmos();
	}

	SDL_RenderPresent(renderer);
}

