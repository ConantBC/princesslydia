#pragma once

#include <list>
#include <map>

#include "Scene.h"

class GameManager
{
public:
	GameManager();
	~GameManager();

	void Start();
	void Update(int dt);
	void Draw();

	void AddScene(const std::string aName, Scene* aScene) { _scenes.insert({ aName, aScene }); }

	//std::list<Scene*> GetScenes() { return _scenes; }

private:
	std::map<std::string, Scene*> _scenes;

	bool drawGizmos = false;

};

