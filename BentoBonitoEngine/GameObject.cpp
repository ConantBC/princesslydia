#include "GameObject.h"

#include "Sprite.h"
#include "Scene.h"

int GameObject::currentID = 0;

std::map<std::string, GameObject::ComponentDefinition> GameObject::_componentsDefinitions;

GameObject::GameObject(const std::string& aName, Scene* pScene, int x, int y, int offsetX, int offsetY) :
	_name(aName), _id(currentID++), _scene(pScene), _x(x), _y(y), _offsetX(offsetX), _offsetY(offsetY)
{
}

GameObject::~GameObject()
{
	delete _sprite;
	for (auto c : _components) delete c;
}

void GameObject::Update(int dt)
{
	if (_sprite) _sprite->Update(dt);
	for (auto c : _components) c->Update(dt);
}

void GameObject::Draw()
{
	if (_sprite != nullptr) _sprite->Draw();
}

void GameObject::SetSprite(Sprite* aSprite)
{
	_sprite = aSprite;
	_sprite->SetOwner(this);
}
