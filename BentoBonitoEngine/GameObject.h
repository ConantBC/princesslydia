#pragma once

#include <list>
#include <string>
#include <map>

#include "Component.h"

#ifdef BENTOBONITOENGINE_EXPORTS
#define BENTO_EXPORT __declspec(dllexport)
#else
#define BENTO_EXPORT __declspec(dllimport)
#endif

class Sprite;
class Scene;
struct Camera;

class BENTO_EXPORT GameObject
{
public:
	static int currentID;

	GameObject(const std::string& aName, Scene* pScene, int x, int y, int offsetX, int offsetY);
	~GameObject();

	void Start() { for (auto c : _components) c->Start(); }
	void Update(int dt);
	void Draw();

	void OnCollisionEnter() { for (auto c : _components) c->OnCollisionEnter(); }
	void OncollisionExit() { for (auto c : _components) c->OnCollisionExit(); }

	void AddComponent(Component * pComponent) { _components.push_back(pComponent); }
	void AddComponent(const std::string & aName) { _components.push_back(_componentsDefinitions[aName]._constructor(this)); }

	void SetSprite(Sprite * aSprite);

	template <class T>
	static Component* CreateConstructor(GameObject * pOwner)
	{
		return new T(pOwner);
	}

	template<class T>
	static void AddComponentDefinition(const std::string & aName)
	{
		ComponentDefinition cd;
		cd._constructor = &CreateConstructor<T>;
		_componentsDefinitions.insert(std::pair<std::string, ComponentDefinition>(aName, cd));
	}

	std::string GetName() { return _name; }
	Scene* GetScene() { return _scene; }

	int GetDrawPositionX() { return _x - _offsetX; }
	int GetDrawPositionY() { return _y - _offsetY; }
	int GetWorldPositionX() { return _x; }
	int GetWorldPositionY() { return _y; }

	Sprite* GetSprite() { return _sprite; }

	template<class T>
	T* GetComponentOfType()
	{
		for (auto c : _components)
		{
			T* p = dynamic_cast<T*>(c);
			if (p) return p;
		}
		return nullptr;
	}

private:
	std::string _name;
	int _id;
	std::list<Component*> _components;
	Sprite* _sprite = nullptr;
	Scene* _scene = nullptr;

	float _x, _y;
	float _offsetX, _offsetY;

	struct ComponentDefinition
	{
		Component* (*_constructor)(GameObject* aOwner);
	};

	static std::map<std::string, ComponentDefinition> _componentsDefinitions;

};
