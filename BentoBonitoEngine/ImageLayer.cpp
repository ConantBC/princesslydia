#include "ImageLayer.h"
#include "GameData.h"
#include "Camera.h"

ImageLayer::ImageLayer(Camera* camera, SDL_Texture* texture) : Layer(camera), _texture(texture)
{
	renderManager = &GameData::GetSingleton()->renderManager;
}

ImageLayer::~ImageLayer()
{
}

void ImageLayer::Draw()
{
	SDL_Rect srcRect = {(_camera->x / _parallax), (_camera->y / _parallax), renderManager->GetWidth(), renderManager->GetHeight()};

	SDL_RenderCopy(renderManager->GetRenderer(), _texture, &srcRect, NULL);
}
