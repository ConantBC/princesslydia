#pragma once

#include <SDL.h>

#include "Layer.h"
#include "RenderManager.h"

class ImageLayer : public Layer
{
public:
	ImageLayer(Camera* camera, SDL_Texture* texture);
	~ImageLayer();

	void SetParallax(int parallax) { _parallax = parallax; }

	void Draw();

private:
	SDL_Texture* _texture;
	RenderManager* renderManager;
};

