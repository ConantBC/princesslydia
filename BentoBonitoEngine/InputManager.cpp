#include "InputManager.h"

#include <cstring>

#include "GameData.h"

InputManager::InputManager(GameData* data) : _data(data)
{
	//TODO: Create a more efficient way to serialize inputs
	_inputs.insert(std::pair<std::string, Input>("Confirm", Input(SDL_SCANCODE_RETURN)));
}

InputManager::~InputManager()
{
}

void InputManager::Update()
{
	memset(keyDown, 0, NUM_BUTTONS);
	anyKey = false;

	SDL_Event e;

	while (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT)
		{
			_data->isRunning = false;
		}
		if (e.type == SDL_KEYDOWN)
		{
			anyKey = true;

			if (e.key.repeat == true)
			{
				continue;
			}

			switch (e.key.keysym.sym)
			{
			case SDLK_RETURN:
				keyDown[BT_START] = true;
				keyDown[BT_CONFIRM] = true;
				break;
			case SDLK_a:
			case SDLK_LEFT:
				keyDown[BT_LEFT] = true;
				break;
			case SDLK_d:
			case SDLK_RIGHT:
				keyDown[BT_RIGHT] = true;
				break;
			case SDLK_w:
			case SDLK_UP:
				keyDown[BT_UP] = true;
				break;
			case SDLK_s:
			case SDLK_DOWN:
				keyDown[BT_DOWN] = true;
				break;
			case SDLK_SPACE:
				keyDown[BT_JUMP] = true;
				keyDown[BT_CONFIRM] = true;
				break;
			case SDLK_ESCAPE:
				keyDown[BT_BACK] = true;
				break;
			}
		}

		_keyboardState = SDL_GetKeyboardState(NULL);
	}
}

bool InputManager::GetKeyDown(Button aButton)
{
	return keyDown[aButton];
}

bool InputManager::GetKey(Button aButton)
{
	switch (aButton)
	{
	case BT_CONFIRM:
		if (_keyboardState[SDL_SCANCODE_RETURN]) return true;
		else return false;
		break;

	case BT_BACK:
		if (_keyboardState[SDL_SCANCODE_ESCAPE]) return true;
		else return false;
		break;
	case BT_START:
		if (_keyboardState[SDL_SCANCODE_RETURN]) return true;
		else return false;
		break;
	case BT_JUMP:
		if (_keyboardState[SDL_SCANCODE_SPACE]) return true;
		else return false;
		break;
	case BT_LEFT:
		if (_keyboardState[SDL_SCANCODE_LEFT] || _keyboardState[SDL_SCANCODE_A]) return true;
		else return false;
		break;
	case BT_RIGHT:
		if (_keyboardState[SDL_SCANCODE_RIGHT] || _keyboardState[SDL_SCANCODE_D]) return true;
		else return false;
		break;
	case BT_UP:
		if (_keyboardState[SDL_SCANCODE_UP] || _keyboardState[SDL_SCANCODE_W]) return true;
		else return false;
		break;
	case BT_DOWN:
		if (_keyboardState[SDL_SCANCODE_DOWN] || _keyboardState[SDL_SCANCODE_S]) return true;
		else return false;
		break;
	default:
		return false;
		break;
	}
	return false;
}
