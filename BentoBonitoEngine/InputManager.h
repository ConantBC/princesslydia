#pragma once

#include <SDL.h>
#include <map>
#include <string>

struct GameData;

struct Input
{
	Input(SDL_Scancode aCode) : code(aCode) {}

	SDL_Scancode code;
};

struct Axis
{

};

enum Button
{
	BT_CONFIRM,
	BT_BACK,
	BT_START,
	BT_JUMP,
	BT_LEFT,
	BT_RIGHT,
	BT_UP,
	BT_DOWN,
	NUM_BUTTONS
};

class InputManager
{
public:
	InputManager(GameData* data);
	~InputManager();

	void Update();

	bool GetKeyDown(Button aButton);
	bool GetKey(Button aButton);

	bool GetLeft() { return GetKey(BT_LEFT); }
	bool GetLeftDown() { return GetKeyDown(BT_LEFT); }
	bool GetRight() { return GetKey(BT_RIGHT); }
	bool GetRightDown() { return GetKeyDown(BT_RIGHT); }
	bool GetPauseDown() { return GetKeyDown(BT_START); }

private:
	GameData* _data;

	std::map<std::string, Input> _inputs;

	bool anyKey = false;
	bool keyDown[NUM_BUTTONS];
	const Uint8* _keyboardState;

};

