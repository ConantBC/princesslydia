#pragma once

#include "Camera.h"

class Layer
{
public:
	Layer(Camera* camera) : _camera(camera) {}
	virtual ~Layer();

	virtual void Draw() = 0;

protected:
	int _parallax = 1;
	Camera* _camera;
};

