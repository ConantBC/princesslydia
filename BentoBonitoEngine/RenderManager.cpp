#include "RenderManager.h"

#include "GameData.h"

#include <iostream>

RenderManager::RenderManager(GameData* data, std::string title, int width, int height, int scale) :
	_data(data), _title(title), _width(width), _height(height), _scale(scale)
{
}

RenderManager::~RenderManager()
{
}

void RenderManager::CreateWindow()
{
	_window = SDL_CreateWindow(_title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _width * _scale, _height * _scale, SDL_WINDOW_OPENGL);
	_renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);

	if (_scale != 1)
	{
		SDL_RenderSetLogicalSize(_renderer, _width, _height);
	}
}

void RenderManager::SetBackgroundColor(int r, int g, int b)
{
	SDL_SetRenderDrawColor(_renderer, r, g, b, 0xff);
}

SDL_Texture* RenderManager::LoadTexture(std::string aPath)
{
	if (_textures.count(aPath) == 1)
	{
		return _textures[aPath];
	}
	else
	{
		SDL_Surface* s = IMG_Load(aPath.c_str());
		if (s == nullptr) std::cout << "Error with Render Manager: " << SDL_GetError() << std::endl;
		SDL_Texture* t = SDL_CreateTextureFromSurface(_renderer, s);

		int w;
		SDL_QueryTexture(t, NULL, NULL, &w, NULL);

		if (t == nullptr) std::cout << "Error with Render Manager: " << SDL_GetError() << std::endl;
		_textures.insert(std::pair<std::string, SDL_Texture*>(aPath, t));
		SDL_FreeSurface(s);
		return t;
	}

	return nullptr;
}
