#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <map>

struct GameData;

class RenderManager
{
public:
	RenderManager(GameData* data, std::string title, int width, int height, int scale);
	~RenderManager();

	void CreateWindow();

	void SetBackgroundColor(int r, int g, int b);

	SDL_Texture* LoadTexture(std::string aPath);

	SDL_Renderer* GetRenderer() { return _renderer; }
	int GetWidth() { return _width; }
	int GetHeight() { return _height; }

private:
	GameData* _data;

	SDL_Renderer* _renderer = nullptr;
	SDL_Window* _window = nullptr;

	std::map<std::string, SDL_Texture*> _textures;

	std::string _title;
	int _width, _height;
	int _scale;
};
