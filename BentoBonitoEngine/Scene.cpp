#include "Scene.h"

#include <SDL.h>
#include <sstream>
#include <iostream>
#include "Box2D/Box2D.h"

#include "GameData.h"
#include "Tilemap.h"
#include "ImageLayer.h"
#include "Tileset.h"
#include "tinyxml2.h"
#include "SpriteSheet.h"
#include "AnimatedSprite.h"

using namespace tinyxml2;

Scene::Scene(const std::string& aName, const std::string& aFilePath) : _name(aName), _camera(0, 0), _world(new b2World(b2Vec2(0.0f, -9.81f)))
{
	std::cout << "Loading map file: " << aFilePath << std::endl << std::endl;

	RenderManager* renderManager = &GameData::GetSingleton()->renderManager;

	XMLDocument doc;
	int e = doc.LoadFile(aFilePath.c_str());
	if (e != 0)
	{
		std::cout << "Error loading map file (" << aFilePath << ") : " << doc.ErrorStr() << std::endl;
	}

	// Setting up global map parameters
	XMLElement* mapNode = doc.FirstChildElement("map");
	mapNode->QueryIntAttribute("width", &_width);
	mapNode->QueryIntAttribute("height", &_height);
	mapNode->QueryIntAttribute("tilewidth", &_tileSize);

	// Set up camera bounds
	int boundsX = (_width * _tileSize) * renderManager->GetWidth();
	int boundsY = (_height * _tileSize) * renderManager->GetHeight();
	_camera.SetBounds(boundsX, boundsY);

	// Create Tilesets
	XMLElement* pChildNode = mapNode->FirstChildElement();
	const char* name = pChildNode->Name();

	// TODO: Add a way to get properties

	while (std::string(pChildNode->Name()) == "tileset")
	{
		// Loading a tileset
		if (std::string(pChildNode->FirstChildElement("properties")->FirstChildElement("property")->Attribute("value")) == "Tileset")
		{
			const char* name;
			const char* filePath;
			int firstGid;
			int tileSize;
			int columns;
			pChildNode->QueryStringAttribute("name", &name);
			pChildNode->QueryIntAttribute("firstgid", &firstGid);
			pChildNode->QueryIntAttribute("tilewidth", &tileSize);
			pChildNode->QueryIntAttribute("columns", &columns);
			pChildNode->FirstChildElement("image")->QueryStringAttribute("source", &filePath);

			std::stringstream ss;
			ss << "assets/" << filePath;

			SDL_Texture* tex = renderManager->LoadTexture(ss.str());
			if (tex == nullptr) std::cout << "Error loading texture for tileset (" << name << ") : " << SDL_GetError() << std::endl;
			_tilesets.push_back(new Tileset(name, firstGid, tileSize, columns, tex));

			std::cout << "Successfully loaded tileset: " << name << std::endl;
		}

		// Loading a Spritesheet
		else if (std::string(pChildNode->FirstChildElement("properties")->FirstChildElement("property")->Attribute("value")) == "Spritesheet")
		{
			const char* name;
			const char* filePath;
			int spriteWidth, spriteHeight;
			pChildNode->QueryStringAttribute("name", &name);
			pChildNode->QueryIntAttribute("tilewidth", &spriteWidth);
			pChildNode->QueryIntAttribute("tileheight", &spriteHeight);
			pChildNode->FirstChildElement("image")->QueryStringAttribute("source", &filePath);

			std::stringstream ss;
			ss << "assets/" << filePath;

			SDL_Texture* tex = renderManager->LoadTexture(ss.str());
			if (tex == nullptr) std::cout << "Error loading texture for spritesheet (" << name << ") : " << SDL_GetError() << std::endl;

			SpriteSheet * pSpriteSheet = new SpriteSheet(tex, spriteWidth, spriteHeight);

			int numSequences = 0;
			XMLElement * pSequence = pChildNode->FirstChildElement("tile");
			while (pSequence)
			{
				const char* name;
				int frameTime;
				int frames = 0;
				pSequence->QueryStringAttribute("type", &name);

				Sequence s;

				XMLElement* pFrame = pSequence->FirstChildElement("animation")->FirstChildElement("frame");
				pFrame->QueryIntAttribute("duration", &s._frameTime);
				while (pFrame)
				{
					SDL_Rect r = { frames * spriteWidth, numSequences * spriteHeight, spriteWidth, spriteHeight };
					s._frames.push_back(r);

					frames++;

					pFrame = pFrame->NextSiblingElement("frame");
				}

				pSpriteSheet->_sequences.insert({ name, s });
				numSequences++;

				pSequence = pSequence->NextSiblingElement("tile");
			}

			_spriteSheets.insert({ name, pSpriteSheet });

			std::cout << "Successfully loaded spritesheet: " << name << std::endl;
		}

		pChildNode = pChildNode->NextSiblingElement();
	}

	// Create Layers
	while (pChildNode)
	{
		// If the layer is an image layer
		if (std::string(pChildNode->Name()) == "imagelayer")
		{
			// Load image
			const char* name;
			const char* filePath;
			pChildNode->QueryStringAttribute("name", &name);
			pChildNode->FirstChildElement("image")->QueryStringAttribute("source", &filePath);

			std::stringstream ss;
			ss << "assets/" << filePath;
			SDL_Texture* tex = renderManager->LoadTexture(ss.str());
			if (tex == nullptr) std::cout << "Error loading background image: " << SDL_GetError() << std::endl;

			ImageLayer * pImageLayer = new ImageLayer(&_camera, tex);

			// Set parallax
			XMLElement * pLayerProperties = pChildNode->FirstChildElement("properties");
			if (pLayerProperties)
			{
				pImageLayer->SetParallax(pLayerProperties->FirstChildElement("property")->IntAttribute("value"));
			}

			_layers.push_back(pImageLayer);

			std::cout << "Successfully loaded image layer: " << name << std::endl;

		}

		// If the layer is a tilemap
		else if (std::string(pChildNode->Name()) == "layer")
		{
			// Load tilemap
			const char* name;
			pChildNode->QueryStringAttribute("name", &name);

			// Create the list of tiles
			Tilemap* pTilemap = new Tilemap(&_camera);
			_layers.push_back(pTilemap);

			// Set parallax if it exists
			XMLElement* pLayerProperties = pChildNode->FirstChildElement("properties");
			if (pLayerProperties)
			{
				pTilemap->SetParallax(pLayerProperties->FirstChildElement("property")->IntAttribute("value"));
			}

			XMLElement* pTile = pChildNode->FirstChildElement("data")->FirstChildElement("tile");

			int tileCounter = 0;
			while (pTile)
			{
				int gid = pTile->IntAttribute("gid");

				// Check if tile is empty
				if (gid == 0)
				{
					tileCounter++;
					pTile = pTile->NextSiblingElement("tile");
					continue;
				}

				// Determine the orientation of this tile
				SDL_RendererFlip flip = SDL_FLIP_NONE;
				if (gid & 0x80000000)
				{
					flip = (SDL_RendererFlip)(flip | SDL_FLIP_HORIZONTAL);
					gid &= ~0x80000000;
				}

				// Find which tileset this tile belongs to
				Tileset * pTileSet = nullptr;
				for (Tileset* ts : _tilesets)
				{
					if (ts->_firstGid <= gid)
					{
						pTileSet = ts;
						break;
					}
				}

				// Skip if no matching tileset is found
				if (pTileSet == nullptr)
				{
					tileCounter++;
					pTile = pTile->NextSiblingElement("tile");
					continue;
				}

				// Find the position on the map
				int posX = (tileCounter % _width) * pTileSet->_tileSize;
				int posY = (tileCounter / _width) * pTileSet->_tileSize;

				// Find the source from the texture
				int srcX = ((gid - 1) % pTileSet->_columns) * pTileSet->_tileSize;
				int srcY = ((gid - 1) / pTileSet->_columns) * pTileSet->_tileSize;

				// Add tile to tileset
				pTilemap->AddTile(pTileSet->_texture, pTileSet->_tileSize, srcX, srcY, posX, posY, flip);

				tileCounter++;
				pTile = pTile->NextSiblingElement("tile");
			}

			std::cout << "Successfully loaded tile layer: " << name << std::endl;
		}

		else if (std::string(pChildNode->Name()) == "objectgroup")
		{
			// Get collision map
			if (std::string(pChildNode->Attribute("name")) == "collisions")
			{
				// Add world collision geometry here
				XMLElement* pColliderNode = pChildNode->FirstChildElement("object");
				while (pColliderNode)
				{
					int x, y, w, h;
					pColliderNode->QueryIntAttribute("x", &x);
					pColliderNode->QueryIntAttribute("y", &y);
					pColliderNode->QueryIntAttribute("width", &w);
					pColliderNode->QueryIntAttribute("height", &h);

					b2PolygonShape* pCollider = new b2PolygonShape();
					pCollider->SetAsBox(w / 2, h / 2, b2Vec2(x + w / 2, y + h / 2), 0.0f);
					_colliders.push_back(pCollider);

					pColliderNode = pColliderNode->NextSiblingElement("object");
				}
			}

			// Get GameObjects
			else if (std::string(pChildNode->Attribute("name")) == "GameObjects")
			{
				XMLElement* pObjectNode = pChildNode->FirstChildElement("object");
				while (pObjectNode)
				{
					// Load gameobjects here!
					const char* name;
					const char* type;
					int x, y;
					pObjectNode->QueryStringAttribute("name", &name);
					pObjectNode->QueryStringAttribute("type", &type);
					pObjectNode->QueryIntAttribute("x", &x);
					pObjectNode->QueryIntAttribute("y", &y);

					// Convert coordinate system from tiled
					y -= _spriteSheets[name]->_height;

					// Spawn the object
					GameObject* pGameObject = new GameObject(name, this, x, y, 0, 0);

					// Set sprite
					pGameObject->SetSprite(new AnimatedSprite(&_camera, _spriteSheets[name], type));

					// Load components
					XMLElement* pComponentNode = pObjectNode->FirstChildElement("properties")->FirstChildElement("property");
					if (pComponentNode)
					{
						// TODO: Check to make sure property is in fact a component
						const char* propertyType;
						pComponentNode->QueryStringAttribute("value", &propertyType);
						pGameObject->AddComponent(propertyType);
						pComponentNode = pComponentNode->NextSiblingElement();
					}

					// Add to scene
					_objects.push_back(pGameObject);

					std::cout << "Loaded GameObject: " << name << std::endl;

					pObjectNode = pObjectNode->NextSiblingElement("object");
				}
			}
		}

		pChildNode = pChildNode->NextSiblingElement();
	}
}

Scene::~Scene()
{
	for (auto s : _spriteSheets) delete s.second;
	for (auto l : _layers) delete l;
	for (auto t : _tilesets) delete t;
	for (auto o : _objects) delete o;
	delete _world;
}

void Scene::Start()
{
	for (GameObject* g : _objects)
	{
		g->Start();
	}
}

void Scene::Update(int dt)
{
	if (!isPaused)
	{
		for (GameObject* g : _objects)
		{
			g->Update(dt);
		}
	}
}

void Scene::Draw()
{
	for (auto l : _layers)
	{
		l->Draw();
	}
	for (auto g : _objects)
	{
		g->Draw();
	}
}

void Scene::DrawGizmos()
{
	SDL_Renderer* renderer = GameData::GetSingleton()->renderManager.GetRenderer();

	int count = _colliders.size();

	SDL_Rect* rects = new SDL_Rect[count];

	int i = 0;
	for (auto c : _colliders)
	{
		rects[i++] = {
			(int)(c->m_vertices[0].x - _camera.x),
			(int)(c->m_vertices[0].y - _camera.y),
			(int)(c->m_vertices[2].x - c->m_vertices[0].x),
			(int)(c->m_vertices[2].y - c->m_vertices[0].y)
		};
	}

	SDL_RenderDrawRects(GameData::GetSingleton()->renderManager.GetRenderer(), rects, count);
	delete[] rects;
}

void Scene::LoadSceneFromXML(const std::string & aFilePath)
{

}
