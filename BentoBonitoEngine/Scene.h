#pragma once

#include <list>
#include <map>

#include "GameObject.h"
#include "Camera.h"

#ifdef BENTOBONITOENGINE_EXPORTS
#define BENTO_EXPORT __declspec(dllexport)
#else
#define BENTO_EXPORT __declspec(dllimport)
#endif

class Tilemap;
class Layer;
class AnimatedSprite;
struct Tileset;
struct SpriteSheet;
class b2World;
class b2PolygonShape;

class BENTO_EXPORT Scene
{
public:
	Scene(const std::string& aName, const std::string& aFilePath);
	~Scene();

	void Start();
	void Update(int dt);
	void Draw();
	void DrawGizmos();

	void SetPause(bool aPaused) { isPaused = aPaused; }

	void AddGameObject(GameObject* aObject) { _objects.push_back(aObject); }

	Camera* GetCamera() { return &_camera; }
	std::list<GameObject*> GetGameObjects() { return _objects; }

	void LoadSceneFromXML(const std::string& aFilePath);

private:
	std::string _name;

	int _width, _height;
	int _tileSize;

	std::list<Tileset*> _tilesets;
	std::map<std::string, SpriteSheet*> _spriteSheets;

	std::list<Layer*> _layers;
	std::list<GameObject*> _objects;
	Camera _camera;

	b2World* _world;
	std::list<b2PolygonShape*> _colliders;

	bool isPaused = false;
};

