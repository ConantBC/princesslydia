#pragma once

#include <vector>

struct Sequence
{
	std::vector<SDL_Rect> _frames;
	int _frameTime;
	bool _loops = true;
};