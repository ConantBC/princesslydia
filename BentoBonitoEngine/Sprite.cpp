#include "Sprite.h"
#include "GameData.h"

#include <SDL.h>
#include <iostream>

Sprite::Sprite(Camera* aCamera) : _camera(aCamera), _flip(SDL_FLIP_NONE),
	_renderer(GameData::GetSingleton()->renderManager.GetRenderer())
{
}

Sprite::~Sprite()
{
}

void Sprite::Draw()
{
	
}
