#pragma once

#include <string>

#ifdef BENTOBONITOENGINE_EXPORTS
#define BENTO_EXPORT __declspec(dllexport)
#else
#define BENTO_EXPORT __declspec(dllimport)
#endif

class GameObject;
struct Camera;
class SDL_Renderer;

class BENTO_EXPORT Sprite
{
public:
	Sprite(Camera* aCamera);
	~Sprite();

	virtual void Update(int dt) {}
	virtual void Draw();

	void SetOwner(GameObject* aGameObject) { _gameObject = aGameObject; }

protected:
	GameObject* _gameObject = nullptr;
	SDL_Renderer* _renderer;
	Camera* _camera = nullptr;

	int _flip;

};

