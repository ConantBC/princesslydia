#pragma once

#include <SDL.h>
#include <map>
#include <string>

#include "Sequence.h"

struct SpriteSheet
{
	SpriteSheet(SDL_Texture* aTexture, int aWidth, int aHeight) : _texture(aTexture), _width(aWidth), _height(aHeight) {}

	SDL_Texture* _texture;
	std::map<std::string, Sequence> _sequences;
	int _width, _height;
};