#include "Tile.h"
#include "GameData.h"

Tile::Tile(SDL_Texture* texture, Camera* camera, int size, int srcX, int srcY, int posX, int posY, SDL_RendererFlip flip, int parallax) :
	_texture(texture), _flip(flip), _camera(camera), _size(size), x(posX), y(posY), _parallax(parallax)
{
	_srcRect.x = srcX;
	_srcRect.y = srcY;
	_srcRect.w = size;
	_srcRect.h = size;

	renderManager = &GameData::GetSingleton()->renderManager;
}

Tile::~Tile()
{
}

void Tile::Draw()
{
	int screenPosX = x - (_camera->x / _parallax);
	int screenPosY = y - (_camera->y / _parallax);

	// Check to make sure tile is visible before drawing it
	if (screenPosX > -_size && screenPosX < renderManager->GetWidth()
		&& screenPosY > -_size && screenPosY < renderManager->GetHeight())
	{
		SDL_Rect destRect = { screenPosX, screenPosY, _size, _size };
		SDL_RenderCopyEx(renderManager->GetRenderer(), _texture, &_srcRect, &destRect, 0.0f, NULL, _flip);
	}
}
