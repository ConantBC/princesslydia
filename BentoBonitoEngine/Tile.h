#pragma once

#include <SDL.h>

#include "Camera.h"
#include "RenderManager.h"

class Tile
{
public:
	Tile(SDL_Texture* texture, Camera* camera, int size, int srcX, int srcY, int posX, int posY, SDL_RendererFlip flip, int parallax);
	~Tile();

	void Draw();

private:
	SDL_Texture* _texture;
	SDL_Rect _srcRect;

	Camera* _camera;
	RenderManager* renderManager;

	int x, y;
	int _size;
	int _parallax;
	SDL_RendererFlip _flip;
};

