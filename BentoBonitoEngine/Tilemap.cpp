#include "Tilemap.h"

#include <cstring>

Tilemap::Tilemap(Camera* camera) : Layer(camera)
{
	
}

Tilemap::~Tilemap()
{
}

void Tilemap::AddTile(SDL_Texture* texture, int size, int srcX, int srcY, int posX, int posY, SDL_RendererFlip flip)
{
	_tiles.push_back(Tile(texture, _camera, size, srcX, srcY, posX, posY, flip, _parallax));
}

void Tilemap::Draw()
{
	for (int i = 0; i < _tiles.size(); i++)
	{
		_tiles[i].Draw();
	}
}
