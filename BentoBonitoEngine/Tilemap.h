#pragma once

#include <SDL.h>
#include <vector>

#include "Tile.h"
#include "Layer.h"
#include "Camera.h"

class Tilemap : public Layer
{
public:
	Tilemap(Camera* camera);
	~Tilemap();

	void AddTile(SDL_Texture* texture, int size, int srcX, int srcY, int posX, int posY, SDL_RendererFlip flip);

	void SetParallax(int parallax) { _parallax = parallax; }

	void Draw();

private:
	std::vector<Tile> _tiles;
};

