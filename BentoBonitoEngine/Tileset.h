#pragma once

#include <SDL.h>
#include <string>

struct Tileset
{
	Tileset() {}
	Tileset(std::string name, int firstGid, int tileSize, int columns, SDL_Texture* texture) :
		_name(name), _firstGid(firstGid), _tileSize(tileSize), _columns(columns), _texture(texture)
	{
	}

	std::string _name;
	int _firstGid;
	int _tileSize;
	int _columns;
	SDL_Texture* _texture;
};

