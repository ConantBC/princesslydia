#pragma once

#include "UIMenu.h"
#include <list>

class UIManager
{
public:
	UIManager();
	~UIManager();

private:
	UIMenu* _activeMenu;
	std::list<UIMenu> _menus;
};

