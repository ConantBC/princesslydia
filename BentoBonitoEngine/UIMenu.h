#pragma once

#include "UIPanel.h"
#include "UIMenuItem.h"
#include <vector>

class UIMenu
{
public:
	UIMenu();
	~UIMenu();

private:
	std::vector<UIMenuItem*> _menuItems;
	UIPanel* _panel = nullptr;
};
