#pragma once

class UIMenu;

class UIMenuItem
{
public:
	UIMenuItem();
	~UIMenuItem();

private:
	UIMenu* _menu;

	// The function that is called when you select this menu item
	void (*Select)(void*);
};

