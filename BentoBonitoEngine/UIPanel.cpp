#include "UIPanel.h"

#include "GameData.h"

UIPanel::UIPanel(SDL_Texture* aTexture, int borderWidth, int x, int y, int w, int h) : _texture(aTexture), _borderWidth(borderWidth)
{
	int srcW, srcY;
	SDL_QueryTexture(aTexture, NULL, NULL, &srcW, &srcY);

	_srcRects[0] = { 0, 0, borderWidth, borderWidth };
	_srcRects[1] = { borderWidth, 0, srcW - 2 * borderWidth, borderWidth };
	_srcRects[2] = { srcW - borderWidth, 0, borderWidth, borderWidth };
	_srcRects[3] = { 0, borderWidth, borderWidth, srcY - 2 * borderWidth };
	_srcRects[4] = { borderWidth, borderWidth, srcW - 2 * borderWidth, srcY - 2 * borderWidth };
	_srcRects[5] = { srcW - borderWidth, borderWidth, borderWidth, srcY - 2 * borderWidth };
	_srcRects[6] = { 0, srcY - borderWidth, borderWidth, borderWidth };
	_srcRects[7] = { borderWidth, srcY - borderWidth, srcW - 2 * borderWidth, borderWidth };
	_srcRects[8] = { srcW - borderWidth, srcY - borderWidth, borderWidth, borderWidth };

	_destRects[0] = { x, y, borderWidth, borderWidth };
	_destRects[1] = { x + borderWidth, y, w - 2 * borderWidth, borderWidth };
	_destRects[2] = { x + w - borderWidth, y, borderWidth, borderWidth };
	_destRects[3] = { x, y + borderWidth, borderWidth, h - 2 * borderWidth };
	_destRects[4] = { x + borderWidth, y + borderWidth, w - 2 * borderWidth, h - 2 * borderWidth };
	_destRects[5] = { x + w - borderWidth, y + borderWidth, borderWidth, h - 2 * borderWidth };
	_destRects[6] = { x, y + h - borderWidth, borderWidth, borderWidth };
	_destRects[7] = { x + borderWidth, y + h - borderWidth, w - 2 * borderWidth, borderWidth };
	_destRects[8] = { x + w - borderWidth, y + h - borderWidth, borderWidth, borderWidth };
}

UIPanel::~UIPanel()
{
}

void UIPanel::SetPosition(int x, int y, int w, int h)
{
	_destRects[0] = { x, y, _borderWidth, _borderWidth };
	_destRects[1] = { x + _borderWidth, y, w - 2 * _borderWidth, _borderWidth };
	_destRects[2] = { x + w - _borderWidth, y, _borderWidth, _borderWidth };
	_destRects[3] = { x, y + _borderWidth, _borderWidth, h - 2 * _borderWidth };
	_destRects[4] = { x + _borderWidth, y + _borderWidth, w - 2 * _borderWidth, h - 2 * _borderWidth };
	_destRects[5] = { x + w - _borderWidth, y + _borderWidth, _borderWidth, h - 2 * _borderWidth };
	_destRects[6] = { x, y + h - _borderWidth, _borderWidth, _borderWidth };
	_destRects[7] = { x + _borderWidth, y + h - _borderWidth, w - 2 * _borderWidth, _borderWidth };
	_destRects[8] = { x + w - _borderWidth, y + h - _borderWidth, _borderWidth, _borderWidth };
}

void UIPanel::Draw()
{
	SDL_Renderer* renderer = GameData::GetSingleton()->renderManager.GetRenderer();

	for (int i = 0; i < 9; i++)
	{
		SDL_RenderCopy(renderer, _texture, &_srcRects[i], &_destRects[i]);
	}
}
