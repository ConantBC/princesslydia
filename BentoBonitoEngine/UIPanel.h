#pragma once

#include <SDL.h>

class UIPanel
{
public:
	UIPanel(SDL_Texture* aTexture, int borderWidth, int x, int y, int w, int h);
	~UIPanel();

	void SetPosition(int x, int y, int w, int h);

	void Draw();

private:
	int _borderWidth;

	SDL_Texture* _texture;
	SDL_Rect _srcRects[9];
	SDL_Rect _destRects[9];

	bool isVisible;
};

