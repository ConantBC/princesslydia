#include "Player.h"

#include "BentoBonitoEngine.h"
#include <iostream>

Player::Player(GameObject* pObject) : Component(pObject)
{
}

Player::~Player()
{
}

void Player::Start()
{
	std::cout << "Player has spawned!\n\n";
	_camera = gameObject->GetScene()->GetCamera();
	_sprite = dynamic_cast<AnimatedSprite*>(gameObject->GetSprite());
}

void Player::Update(int dt)
{
	if (_camera)
	{
		float cameraDX = (-Bento_GetLeft() + Bento_GetRight()) * (float)dt / 1000.0f;
		_camera->x += cameraDX * _speed;
		_camera->Clamp();
	}
}
