#pragma once

#include "Scene.h"
#include "GameObject.h"
#include "Component.h"
#include "Camera.h"
#include "AnimatedSprite.h"


class Player : public Component
{
public:
	Player(GameObject* pObject);
	~Player();

	void Start();
	void Update(int dt);

	void OnCollisionEnter() {}
	void OnCollisionExit() {}

private:
	Camera* _camera = nullptr;
	AnimatedSprite* _sprite = nullptr;

	float _speed = 80;
};

