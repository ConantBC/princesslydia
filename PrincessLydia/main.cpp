#include "BentoBonitoEngine.h"

#include "Player.h"

int main(int argc, char** argv)
{
	Bento_CreateGame();

	GameObject::AddComponentDefinition<Player>("Player");
	Bento_LoadScene("Scene1", "assets/PrincessLydia.tmx");

	Bento_Run();
	Bento_Close();

	return 0;
}