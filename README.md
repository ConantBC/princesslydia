# Princess Lydia

Princess Lydia is a 2D platformer game in the style of old Game Boy games. The game is being developed using a proprietary engine, and is still very much a work in progress.

You are free to check it out while I work on it, but there is no documentation and I will offer no support until the game is completed.


## Credits

Princess Lydia is being developed with the following third-party libraries

- [SDL2](https://www.libsdl.org/index.php)
- [TinyXML2](http://www.grinninglizard.com/tinyxml2/)
- [Box2D](https://box2d.org/)
 
With help from [Tiled](https://www.mapeditor.org/) to build levels.